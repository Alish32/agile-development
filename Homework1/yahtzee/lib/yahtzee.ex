defmodule Yahtzee do
  def score_upper(dice) do
    %{Ones: length(Enum.filter(dice, fn e -> e == 1 end)),
      Twos: length(Enum.filter(dice, fn e -> e == 2 end)),
      Threes: length(Enum.filter(dice, fn e -> e == 3 end)),
      Fours: length(Enum.filter(dice, fn e -> e == 4 end)),
      Fives: length(Enum.filter(dice, fn e -> e == 5 end)),
      Sixes: length(Enum.filter(dice, fn e -> e == 6 end))}
  end

  def isFullHouse(d) do
    unique1 = Enum.uniq(d)
    ondupes = d -- unique1
    unique2 = Enum.uniq(ondupes)
    asdf = unique1 -- unique2
    if Enum.count(asdf) == 0 and Enum.count(unique1)>1 do 25 else 0 end
  end

  def four(d) do
    unique = Enum.uniq(d)
    ondupes = unique -- Enum.uniq(d -- unique)
    if Enum.count(ondupes) == 1  do Enum.sum(d) else 0 end
  end

  def three(d) do
    unique = Enum.uniq(d)
    ondupes = unique -- Enum.uniq(d -- unique)
    if Enum.count(ondupes) == 2  do Enum.sum(d) else 0 end
  end

  def isSmallStraight(d) do
    d = Enum.join(Enum.sort(Enum.uniq(d)))
    if(d =~ "1234" || d =~ "2345" || d =~ "3456") do 30 else 0 end
  end

  def largeStraight(d) do
    d = Enum.join(Enum.sort(Enum.uniq(d)))
    if(d =~ "12345" || d =~ "23456") do 40 else 0 end
  end

  def isYahtzee(d) do
    if(Enum.count(Enum.uniq(d)) == 1) do 50 else 0 end
  end
  
  def chance(d) do
    a = Enum.uniq(d)
    a = d--a
    a = Enum.uniq(a)
    if Enum.count(a)==2  do Enum.sum(d) else 0 end
  end

  def score_lower(dice) do
    %{"Three of a kind": three(dice),
      "Four of a kind": four(dice),
      "Full house": isFullHouse(dice),
      "Small straight": isSmallStraight(dice),
      "Large straight": largeStraight(dice),
      "Yahtzee": isYahtzee(dice),
      "Chance": chance(dice)}
  end
end